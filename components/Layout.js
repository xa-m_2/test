import NavBar from './Navbar';
import Footer from './Footer';

const Layout = (props) => {
  return (
    <div className='context'>
      <NavBar />
      {props.children}
      <Footer />
    </div>
  );
};

export default Layout;
