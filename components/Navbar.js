import Link from 'next/link';

import React from 'react';

const NavBar = () => {
  return (
    <nav>
      <div className='logo'>
        <h1>xa-m</h1>
      </div>
      <Link href='/'>
        <a>Home </a>
      </Link>
      <Link href='/about'>
        <a>About </a>
      </Link>
      <Link href='/xa-m'>
        <a>xa-m </a>
      </Link>
    </nav>
  );
};

export default NavBar;
