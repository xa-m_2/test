import Link from 'next/link';

const NotFound = () => {
  return (
    <div className='not-found'>
      <h1>Seems Lost...</h1>
      <h2>There is nothing. this page is never exists...</h2>
      <Link href='/'>
        <a>Go back to Safe</a>
      </Link>
    </div>
  );
};

export default NotFound;
