import Head from 'next/head';
import Image from 'next/image';
import styles from '../styles/Home.module.css';
import NavBar from './../components/Navbar';
import Footer from './../components/Footer';
import Link from 'next/link';

export default function Home() {
  return (
    <>
      <Head>
        <title>Ninja'dan aldım</title>
      </Head>
      <div>
        <h1 className={styles.title}>Home Page</h1>
        <p className={styles.text}>
          You can set up a basic GitHub Pages site for yourself, your
          organization, or your project.
        </p>
        <p className={styles.text}>
          You can use Jekyll, a popular static site generator, to further
          customize your GitHub Pages site.
        </p>
        <p className={styles.text}>
          You can customize the domain name of your GitHub Pages site.
        </p>

        <Link href='/xa-m'>
          <a className={styles.btn}>Go to somewhere</a>
        </Link>
      </div>
    </>
  );
}
