import styles from '../../styles/xa-m.module.css';
import Link from 'next/link';

export const getStaticProps = async () => {
  const res = await fetch('https://jsonplaceholder.typicode.com/users');
  const data = await res.json();

  return {
    props: { mami: data },
  };
};

const Mami = (props) => {
  // console.log(props.mami);
  return (
    <div>
      <h1>Mamiiiiiii</h1>
      {props.mami.map((mami) => (
        <div key={mami.id}>
          <Link href={'/xa-m/' + mami.id}>
            <a className={styles.single}>
              <h3>{mami.name}</h3>
            </a>
          </Link>
        </div>
      ))}
    </div>
  );
};

export default Mami;
